<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "data".
 *
 * @property int $id
 * @property string $CompanyName
 * @property string $PhoneNumber
 * @property string $Address
 * @property string $Website
 */
class Data extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'data';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CompanyName', 'PhoneNumber', 'Address', 'Website'], 'required'],
            [['CompanyName', 'PhoneNumber', 'Address', 'Website'], 'string', 'max' => 225],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'CompanyName' => 'Company Name',
            'PhoneNumber' => 'Phone Number',
            'Address' => 'Address',
            'Website' => 'Website',
        ];
    }
}
